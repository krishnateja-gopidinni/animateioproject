import { BrowserModule } from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';

import { AppComponent } from './app.component';
import {FilterPipeModule} from 'ngx-filter-pipe';
import {ngxMediumModule} from 'ngx-medium-editor';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {EllipsisPipe, SearchTextBoldPipe} from './common/pipe';

@NgModule({
  declarations: [
    AppComponent,
    SearchTextBoldPipe,
    EllipsisPipe
  ],
  imports: [
    BrowserModule,
    FilterPipeModule,
    ngxMediumModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ],
})
export class AppModule { }
