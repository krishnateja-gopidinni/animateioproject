/**
 * common pipe that returns bold text for user search data
 */
import {Pipe, PipeTransform} from '@angular/core';
import {CommonUtils} from './CommonUtils';

@Pipe({name: 'searchTextBold'})
export class SearchTextBoldPipe implements PipeTransform {
  transform(value: string, args: string): any {
    if (value && args) {
      if (value.toLowerCase().includes(args.toLowerCase())) {
        value =  value.replace(args, `<b>${args}</b>`);
        if (value.includes(CommonUtils.getCapitalizeString(args))) {
          value = value.replace(args, `<b>${args}</b>`);
        }
        return value;
      } else {
        return  value;
      }
    } else {
      return  value;
    }
  }
}

@Pipe({name:"ellipsis"})
export class EllipsisPipe implements  PipeTransform{
  transform(val, params) {
    if (undefined === params)
      return val;

    return (val.length > params) ? val.substring(0, params) + '...' : val;
  }
}
