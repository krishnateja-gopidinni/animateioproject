import {Injectable} from '@angular/core';

@Injectable({
  providedIn:'root'
})
export class CommonUtils {

  /**
   * function for capatalize string
   * @param sentence
   * @returns {string}
   */
  static getCapitalizeString(sentence) {
    if (sentence) {
      const _value = sentence.toLowerCase();
      return _value.replace(/\w+/g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1);
      }).replace(/\s/g, ' ');
    }
  }

}
